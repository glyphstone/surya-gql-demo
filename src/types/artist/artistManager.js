import { BaseCRUDManager } from "@brownpapertickets/surya-gql-types"

const authorized = {
  get: { groups: ["*"] },
  query: { groups: ["*"] },
  create: { groups: ["admin"] },
  update: { groups: ["admin"], owner: true },
  delete: { groups: ["admin"] },
}

export class ArtistManager extends BaseCRUDManager {
  authorized() {
    return authorized
  }
}
