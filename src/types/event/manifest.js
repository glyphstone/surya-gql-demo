export const Manifest = {
  sysVersion: "1.0.0",
  typeVersion: "0.1.0",
  description: "PGSQl based Events",
  types: ["Event"],
  queries: "EventQueries",
  resolvers: "EventResolvers",
  manager: "EventManager",
  data: {
    interface: "PGSql",
    api: "EventAPI",
    config: { table: "events" },
  },
}
