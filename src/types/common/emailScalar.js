import { GraphQLScalarType } from "graphql"
import { Kind } from "graphql"

export const EmailScalar = {
  EmailScalar: new GraphQLScalarType({
    name: "EmailScalar",
    description: "A validated string matching for email",
    serialize(value) {
      console.log(`Email.serialize(${value})`)
      let result = `mailto:[${value}]`
      return result
    },
    parseValue(value) {
      console.log(`Email.parseValue(${value})`)
      let result = value
      return result
    },
    parseLiteral(ast) {
      console.log(`Email.parseSerial(${JSON.stringify(ast)})`)
      switch (ast.kind) {
        case Kind.STRING:
          const value = ast.value
          if (value.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
            return value
          } else {
            throw new Error(`Invalid Email: ${value}`)
          }
        default:
          console.log(`Type not supported: ${ast.kind}`)
          throw new Error(`EmailScalar type not supported: ${ast.kind}`)
      }
    },
  }),
}
