export const EnumResolvers = {
  ENUM_RGB: {
    RED: "#f00",
    GREEN: "#0f0",
    BLUE: "#00f",
  },

  ENUM_GENDER: {
    TRANSGENDER: "TG",
    NON_BINARY: "NB",
    OTHER: "O",
    UNKNOWN: "?",
    FEMALE: "F",
    MALE: "M",
  },

  ENUM_COUNTRIES: {
    USA: "US",
    CANADA: "CA",
    MEXICO: "MX",
    UNITED_KINGDOM: "UK",
    GERMANY: "DE",
    FRANCE: "FR",
    SPAIN: "ES",
    INDIA: "IN",
    CHINA: "CN",
  },
}
