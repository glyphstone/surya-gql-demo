import express from "express"
import { ApolloServer } from "apollo-server-express"
import { MockLog } from "./util/mockLog"
import { TypesManager } from "@brownpapertickets/surya-gql-types"
import { MongoDB } from "@brownpapertickets/surya-gql-data-mongodb"
import { PGSql } from "@brownpapertickets/surya-gql-data-pgsql"
import { DefaultSessionManager } from "@brownpapertickets/surya-gql-auth"

class SuryaDemoServer {
  async run() {
    const isProduction = false
    const mongoConnection = "mongodb://localhost/bpt"
    const pgsqlConnection = "postgresql://localhost/bpt"
    const app = express()
    const port = 3003
    const log = new MockLog()
    let container = { log }

    // Create data interfaces and put in container for TypesManager reference
    const mongoDB = new MongoDB(container)
    const pgSql = new PGSql(container)

    container.dataInterfaces = { MongoDB: mongoDB, PGSql: pgSql }

    // Create an AuthSessionManager object that maps credentials (token or basic) to a user session object.
    const sessionManager = new DefaultSessionManager(container)
    container.sessionManager = sessionManager // differentiate from legacy

    // Create the TypesManager and load up Types.
    const typesManager = new TypesManager(container)
    try {
      await typesManager.loadTypeModules(__dirname + "/types")
    } catch (ex) {
      log.error(`Error loading types modules: ${ex}`)
    }

    // Connect data interfaces to data sources.
    try {
      await mongoDB.connect(mongoConnection, isProduction)
    } catch (ex) {
      log.error(`Error connecting to MongoDB: ${mongoConnection}: ${ex}`)
    }
    try {
      await pgSql.connect(pgsqlConnection, isProduction)
    } catch (ex) {
      log.error(`Error connecting to PGSql: ${pgsqlConnection}: ${ex}`)
    }

    // get the goods from the TypesManager for ApolloServer
    let typeDefs = typesManager.getTypeDefs()
    const resolvers = typesManager.getResolvers()
    const managers = typesManager.getManagers()

    let server
    try {
      server = new ApolloServer({
        typeDefs,
        resolvers,
        debug: true,
        context: async ({ req }) => {
          let auth = (req.headers && req.headers.authorization) || ""
          const token = auth.substring("Bearer".length).trim()
          return {
            log,
            managers,
            auth: { token },
          }
        },
      })
    } catch (ex) {
      log.error(`Error creating ApolloServer`)
      log.info(`Schema: ${typeDefs}`)
      throw ex
    }

    server.applyMiddleware({ app })

    app.listen({ port }, () =>
      log.info(
        `🚀 Server ready at http://localhost:${port}${server.graphqlPath}`
      )
    )
  }
}

const server = new SuryaDemoServer()

server
  .run()
  .then((exitCode) => {
    process.exitCode = exitCode
  })
  .catch((error) => {
    console.log(error)
    process.exit(200)
  })
